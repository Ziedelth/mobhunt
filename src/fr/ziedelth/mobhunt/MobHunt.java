package fr.ziedelth.mobhunt;

import fr.ziedelth.mobhunt.commands.HuntCommand;
import fr.ziedelth.mobhunt.listeners.EventManager;
import fr.ziedelth.mobhunt.utils.Hunter;
import fr.ziedelth.mobhunt.utils.JSONUtils;
import fr.ziedelth.mobhunt.utils.MobHuntTimer;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MobHunt extends JavaPlugin {
    private static MobHunt instance;
    private String winHuntCommand = "";
    private int timeBetweenHunt;
    private int timeDuringHunt;
    private List<String> worlds;
    private List<Hunter> hunters = new ArrayList<>();
    private boolean isActivated = false;
    private boolean canJoinHunt = false;
    private MobHuntTimer timer;

    public static MobHunt getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        super.onLoad();
        instance = this;
    }

    @Override
    public void onEnable() {
        super.onEnable();

        File folder = this.getDataFolder();
        // Si le dossier parent du plugin n'existe pas
        if (!folder.exists()) folder.mkdirs();
        File config_file = new File(folder, "config.json");

        // Si le fichier de configuration n'existe pas
        if (!config_file.exists()) {
            try {
                this.getLogger().info("Creating config file...");
                config_file.createNewFile();
                JSONObject object = new JSONObject();
                // Génération des paramètres de défaut
                object.put("win_hunt_command", "eco give %player% 250");
                object.put("time_between_hunt", 3600);
                object.put("time_during_hunt", 600);
                object.put("set_time_on_load", true);
                object.put("time_on_load", 0);
                List<String> worlds = new ArrayList<>();
                worlds.add("world_resources");
                worlds.add("the_nether");
                object.put("worlds", worlds);
                JSONUtils.write(config_file, object);
                this.getLogger().info("Config file has been created!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Chargement de la configuration du plugin
        try {
            this.getLogger().info("Loading config...");
            this.getLogger().info("-----------------------------------------------------------------");
            JSONObject config_object = JSONUtils.read(config_file);
            this.winHuntCommand = (String) config_object.get("win_hunt_command");
            this.getLogger().info("Win hunt command: \"" + this.winHuntCommand + "\"");
            this.timeBetweenHunt = Integer.parseInt(config_object.get("time_between_hunt").toString());
            this.getLogger().info("Time between hunt: " + this.timeBetweenHunt + "s");
            this.timeDuringHunt = Integer.parseInt(config_object.get("time_during_hunt").toString());
            this.getLogger().info("Time during hunt: " + this.timeDuringHunt + "s");
            boolean setTimeOnLoad = (boolean) config_object.get("set_time_on_load");
            this.getLogger().info("Set time on load: " + setTimeOnLoad);
            long timeOnLoad = Long.parseLong(config_object.get("time_on_load").toString());
            this.getLogger().info("Time on load: " + timeOnLoad);
            JSONArray json_array = (JSONArray) config_object.get("worlds");
            List<String> list = new ArrayList<>();
            json_array.forEach(o -> list.add((String) o));
            this.worlds = list;
            this.getLogger().info("Worlds:");
            list.forEach(s -> this.getLogger().info("  - " + s));
            this.getLogger().info("-----------------------------------------------------------------");
            this.getLogger().info("Config has been loaded!");

            // Synchronisation de tout les mondes
            if (setTimeOnLoad) {
                this.worlds.forEach(s -> {
                    World world = Bukkit.getWorld(s);
                    if (world == null) return;
                    world.setTime(timeOnLoad);
                });
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        this.getCommand("hunt").setExecutor(new HuntCommand());
        new EventManager(this);
        this.timer = new MobHuntTimer();
        this.timer.start();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public String getWinHuntCommand() {
        return winHuntCommand;
    }

    public int getTimeBetweenHunt() {
        return timeBetweenHunt;
    }

    public int getTimeDuringHunt() {
        return timeDuringHunt;
    }

    public List<String> getWorlds() {
        return worlds;
    }

    public boolean contains(Player player) {
        for (Hunter hunters : Hunter.getPlayers())
            if (hunters.getPlayer().getUniqueId().equals(player.getUniqueId())) return true;
        return false;
    }

    public List<Hunter> getHunters() {
        return hunters;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public boolean canJoinHunt() {
        return canJoinHunt;
    }

    public void setCanJoinHunt(boolean canJoinHunt) {
        this.canJoinHunt = canJoinHunt;
    }

    public MobHuntTimer getTimer() {
        return timer;
    }
}
