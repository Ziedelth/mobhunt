package fr.ziedelth.mobhunt.commands;

import fr.ziedelth.mobhunt.MobHunt;
import fr.ziedelth.mobhunt.utils.ChatUtils;
import fr.ziedelth.mobhunt.utils.Hunter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class HuntCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        boolean isAPlayer = sender instanceof Player;
        boolean hasPermission = isAPlayer && sender.hasPermission("mobhunt.command.event");

        if (args.length == 0) {
            sender.sendMessage(ChatUtils.getPrefix() + "/hunt join §f- §6Permet de participer à la chasse");
            sender.sendMessage(ChatUtils.getPrefix() + "/hunt leave §f- §6Permet de se retirer de la chasse en cours");
            sender.sendMessage(ChatUtils.getPrefix() + "/hunt top §f- §6Affiche le TOP 5 des joueurs de la chasse en cours");
            sender.sendMessage(ChatUtils.getPrefix() + "/hunt gtop §f- §6Affiche le TOP 10 des meilleurs joueurs §o(toutes chasses confondues)");
            sender.sendMessage(ChatUtils.getPrefix() + "/hunt timer §f- §6Affiche le temps restant avant la prochaine chasse");
            sender.sendMessage(ChatUtils.getPrefix() + "/hunt event §f- §6Lancement d'une chasse évènement par un administrateur");
            return false;
        }

        /*
        JOIN
         */
        if (args[0].equalsIgnoreCase("join")) {
            if (!isAPlayer) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cVous n'êtes pas un joueur!");
                return false;
            }

            Player player = (Player) sender;

            if (!MobHunt.getInstance().canJoinHunt()) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cVous ne pouvez pas rejoindre cette chasse pour le moment !");
                return false;
            }

            if (MobHunt.getInstance().contains(player)) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cVous participez déjà à cette chasse !");
                return false;
            }

            MobHunt.getInstance().getHunters().add(Hunter.get(player));
            sender.sendMessage(ChatUtils.getPrefix() + "Vous participerez à la chasse !");
        }
        /*
        LEAVE
         */
        else if (args[0].equalsIgnoreCase("leave")) {
            if (!isAPlayer) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cVous n'êtes pas un joueur!");
                return false;
            }

            Player player = (Player) sender;

            if (!MobHunt.getInstance().contains(player)) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cVous ne participez pas à cette chasse !");
                return false;
            }

            Hunter hunter = Hunter.get(player);
            hunter.save();
            MobHunt.getInstance().getHunters().remove(hunter);
            sender.sendMessage(ChatUtils.getPrefix() + "Vous avez quitté la chasse !");
        }
        /*
        TOP
         */
        else if (args[0].equalsIgnoreCase("top")) {
            if (!MobHunt.getInstance().isActivated()) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cIl n'y a aucune chasse en cours !");
                return false;
            }

            if (Hunter.getPlayers().isEmpty()) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cIl n'y a aucun participants pour cette chasse");
                return false;
            }

            List<Hunter> list = Hunter.getCurrentHuntTOP();
            sender.sendMessage(ChatUtils.getPrefix() + "Classement actuel de la chasse :");

            for (int i = 0; i < 5; i++) {
                if (!(list.size() >= i + 1)) continue;
                Hunter hunter = list.get(i);
                sender.sendMessage(ChatUtils.getPrefix() + "§6" + hunter.getPlayer().getName() + " §eest " + (i == 0 ? "§a" + (i + 1) + "er" : "§c" + (i + 1) + "ème") + " §eavec un total de §6" + hunter.getScore() + " points");
            }
        }
        /*
        GTOP
         */
        else if (args[0].equalsIgnoreCase("gtop")) {
            List<Hunter.SavedHunter> list = Hunter.getGlobalHuntTOP();
            sender.sendMessage(ChatUtils.getPrefix() + "Classement global des chasses :");

            for (int i = 0; i < 10; i++) {
                if (!(list.size() >= i + 1)) continue;
                Hunter.SavedHunter hunter = list.get(i);
                OfflinePlayer player = Bukkit.getOfflinePlayer(hunter.getUuid());
                sender.sendMessage(ChatUtils.getPrefix() + "§6" + player.getName() + " §eest " + (i == 0 ? "§a" + (i + 1) + "er" : "§c" + (i + 1) + "ème") + " §eavec un total de §6" + hunter.getGlobalScore() + " points");
            }
        }
        /*
        TIMER
         */
        else if (args[0].equalsIgnoreCase("timer")) {
            if (MobHunt.getInstance().isActivated()) {
                sender.sendMessage(ChatUtils.getPrefix() + "§cUne chasse est en cours !");
                return false;
            }

            sender.sendMessage(ChatUtils.getPrefix() + "La prochaine chasse débute dans §6" + format(MobHunt.getInstance().getTimer().getRemaningSeconds()) + " §e!");
        }
        /*
        EVENT
         */
        else if (args[0].equalsIgnoreCase("event")) {
            if (hasPermission) {
                if (MobHunt.getInstance().canJoinHunt()) {
                    sender.sendMessage(ChatUtils.getPrefix() + "§cUne chasse va bientôt commencé !");
                    return false;
                }

                if (MobHunt.getInstance().isActivated()) {
                    sender.sendMessage(ChatUtils.getPrefix() + "§cUne chasse est en cours !");
                    return false;
                }

                sender.sendMessage(ChatUtils.getPrefix() + "Vous avez lancé une chasse !");
                MobHunt.getInstance().getTimer().setRemainingSeconds(301);
            } else sender.sendMessage(ChatUtils.getPrefix() + "§cVous n'avez pas la permission d'éxécuter cette commande !");
        }

        return true;
    }

    private String format(long time) {
        long last = time;
        return (last > 3600 ? (String.format("%.2fh", (double) last / 3600D)) : last > 60 ? (String.format("%.2fm", (double) last / 60)) : last > 0 ? (String.format("%.0fs", (double) last)) : String.format("%.0fms", (double) time));
    }
}
