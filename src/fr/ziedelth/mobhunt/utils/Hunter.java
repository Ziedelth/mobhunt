package fr.ziedelth.mobhunt.utils;

import fr.ziedelth.mobhunt.MobHunt;
import fr.ziedelth.mobhunt.builders.ScoreboardBuilder;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Hunter {
    private static final List<Hunter> players = new ArrayList<>();
    private final Player player;
    public int hunts;
    public int globalScore;
    public int mobsKilled;
    public int huntsWin;
    private int score;
    private Scoreboard oldScoreboard;
    private ScoreboardBuilder builder;

    public Hunter(Player player) {
        this.player = player;

        this.oldScoreboard = this.player.getScoreboard();
        this.builder = new ScoreboardBuilder("§cCHASSE");
        this.builder.addObjective(DisplaySlot.SIDEBAR);
        this.builder.buildTo(this.player);

        SavedHunter savedHunter = load(this.player.getUniqueId());
        this.hunts = savedHunter.hunts;
        this.globalScore = savedHunter.globalScore;
        this.mobsKilled = savedHunter.mobsKilled;
        this.huntsWin = savedHunter.huntsWin;
    }

    public static List<Hunter> getPlayers() {
        return players;
    }

    public static Hunter get(Player player) {
        Hunter hunter = null;

        for (Hunter hunters : players) {
            if (hunters.player.getUniqueId().equals(player.getUniqueId())) {
                hunter = hunters;
                break;
            }
        }

        if (hunter == null) {
            hunter = new Hunter(player);
            players.add(hunter);
        }

        return hunter;
    }

    private static File getFile() {
        File folder = MobHunt.getInstance().getDataFolder();
        if (!folder.exists()) folder.mkdirs();
        File players_file = new File(folder, "players.json");

        if (!players_file.exists()) {
            try {
                players_file.createNewFile();
                JSONUtils.write(players_file, new JSONObject());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return players_file;
    }

    private static SavedHunter load(UUID uuid) {
        return get(uuid);
    }

    private static SavedHunter get(UUID uuid) {
        List<SavedHunter> list = getSavedHunters().stream().filter(savedHunter -> savedHunter.uuid.equals(uuid)).collect(Collectors.toList());
        return list.isEmpty() ? new SavedHunter(uuid, 0, 0, 0, 0) : list.get(0);
    }

    public static List<SavedHunter> getSavedHunters() {
        List<SavedHunter> list = new ArrayList<>();

        try {
            JSONObject object = JSONUtils.read(getFile());

            object.forEach((o, o2) -> {
                UUID uuid = UUID.fromString(o.toString());
                JSONObject profile = (JSONObject) o2;
                int hunts = Integer.parseInt(profile.get("hunts").toString());
                int globalScore = Integer.parseInt(profile.get("global-score").toString());
                int mobsKilled = Integer.parseInt(profile.get("mobs-killed").toString());
                int huntsWin = Integer.parseInt(profile.get("hunts-win").toString());
                list.add(new SavedHunter(uuid, hunts, globalScore, mobsKilled, huntsWin));
            });
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return list;
    }

    public static List<Hunter> getCurrentHuntTOP() {
        Map<Hunter, Integer> map = new HashMap<>();
        players.forEach(hunter -> map.put(hunter, hunter.score));
        List<Hunter> sorted = new ArrayList<>();
        //Use Comparator.reverseOrder() for reverse ordering
        map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).forEachOrdered(x -> sorted.add(x.getKey()));
        return sorted;
    }

    public static List<SavedHunter> getGlobalHuntTOP() {
        Map<SavedHunter, Integer> map = new HashMap<>();
        getSavedHunters().forEach(hunter -> map.put(hunter, hunter.globalScore));
        List<SavedHunter> sorted = new ArrayList<>();
        //Use Comparator.reverseOrder() for reverse ordering
        map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).forEachOrdered(x -> sorted.add(x.getKey()));
        return sorted;
    }

    public void save() {
        try {
            JSONObject object = JSONUtils.read(getFile());
            object.put(this.player.getUniqueId().toString(), new SavedHunter(this.player.getUniqueId(), this.hunts, this.globalScore, this.mobsKilled, this.huntsWin).toJSON());
            JSONUtils.write(getFile(), object);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public void setOldScoreboard() {
        this.player.setScoreboard(this.oldScoreboard);
        this.builder.removeTo(this.player);
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
        this.globalScore += score;
        Hunter.getPlayers().forEach(Hunter::us);
    }

    public void us() {
        this.builder.resetDisplayScore();
        List<Hunter> list = getCurrentHuntTOP();

        for (int i = 0; i < 15; i++) {
            if (!(list.size() >= i + 1)) continue;
            Hunter hunter = list.get(i);
            this.builder.addDisplayScore("§e" + (i + 1) + ". §6" + hunter.getPlayer().getName());
        }
    }

    public int getGlobalScore() {
        return globalScore;
    }

    public int getHunts() {
        return hunts;
    }

    public int getHuntsWin() {
        return huntsWin;
    }

    public int getMobsKilled() {
        return mobsKilled;
    }

    public int addMobKill(Entity entity) {
        int score = 0;
        this.mobsKilled++;

        switch (entity.getType()) {
            case BAT:
                this.addScore(score = 5);
                break;
            case ZOMBIE:
            case SKELETON:
            case SPIDER:
            case WOLF:
            case SLIME:
            case CAVE_SPIDER:
            case SILVERFISH:
            case GUARDIAN:
                this.addScore(score = 10);
                break;
            case ELDER_GUARDIAN:
            case SKELETON_HORSE:
                this.addScore(score = 15);
                break;
            case CREEPER:
            case WITCH:
                if (entity instanceof Creeper && ((Creeper) entity).isPowered()) this.addScore(score = 25);
                else this.addScore(score = 20);
                break;
            case ENDERMAN:
                this.addScore(score = 30);
                break;
            default:
                break;
        }

        return score;
    }

    public static class SavedHunter {
        private final UUID uuid;
        private final int hunts, globalScore, mobsKilled, huntsWin;

        public SavedHunter(UUID uuid, int hunts, int globalScore, int mobsKilled, int huntsWin) {
            this.uuid = uuid;
            this.hunts = hunts;
            this.globalScore = globalScore;
            this.mobsKilled = mobsKilled;
            this.huntsWin = huntsWin;
        }

        public UUID getUuid() {
            return uuid;
        }

        public int getHunts() {
            return hunts;
        }

        public int getGlobalScore() {
            return globalScore;
        }

        public int getMobsKilled() {
            return mobsKilled;
        }

        public int getHuntsWin() {
            return huntsWin;
        }

        public JSONObject toJSON() {
            JSONObject object = new JSONObject();
            object.put("hunts", this.hunts);
            object.put("global-score", this.globalScore);
            object.put("mobs-killed", this.mobsKilled);
            object.put("hunts-win", this.huntsWin);
            return object;
        }
    }
}
