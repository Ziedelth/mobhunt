package fr.ziedelth.mobhunt.utils;

import fr.ziedelth.mobhunt.MobHunt;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MobHuntTimer extends Timer {
    private Player first;

    public int getRemaningSeconds() {
        return MobHunt.getInstance().getTimeBetweenHunt() - this.getSeconds();
    }

    public void setRemainingSeconds(int seconds) {
        this.seconds = MobHunt.getInstance().getTimeBetweenHunt() - seconds;
    }

    @Override
    public void run() {
        if (!MobHunt.getInstance().isActivated()) {
            int seconds = getRemaningSeconds();

            if (seconds == 3600 || seconds == 1800 || seconds == 600 || seconds == 300 || seconds == 120 || seconds == 60)
                this.sendMessage("La prochaine chasse commencera dans " + (seconds / 60) + " minute" + ((seconds / 60) > 1 ? "s" : "") + " !");

            if (seconds <= 300) {
                MobHunt.getInstance().setCanJoinHunt(true);
                if (seconds == 300)
                    this.sendMessage("Une chasse aux monstres va être lancée, faites §6/hunt join §epour y participer !");
            }

            if (seconds <= 0) {
                MobHunt.getInstance().setCanJoinHunt(false);
                MobHunt.getInstance().setActivated(true);
                Hunter.getPlayers().forEach(hunter -> hunter.hunts++);
                this.sendMessage("La chasse démarre, bonne chance à tous !");
                this.resetSeconds();
            }
        } else {
            int seconds = MobHunt.getInstance().getTimeDuringHunt() - this.getSeconds();
            Hunter.getPlayers().forEach(Hunter::save);

            if (!this.allHuntersHaveTheSameScore()) {
                Player first = this.getFirst();

                if (this.first == null || !first.getUniqueId().equals(this.first.getUniqueId())) {
                    this.first = first;
                    this.sendPrivateMessage(this.first.getName() + " est maintenant en tête du classement !");
                }
            }

            if (seconds <= 0) {
                MobHunt.getInstance().setActivated(false);
                this.resetSeconds();
                this.first = null;

                if (!Hunter.getPlayers().isEmpty()) {
                    Player first = this.getFirst();
                    Hunter.getPlayers().forEach(Hunter::setOldScoreboard);
                    Hunter.getPlayers().clear();
                    this.sendMessage("La chasse est terminée, " + first.getName() + " remporte la partie et empoche la récompense !");
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), MobHunt.getInstance().getWinHuntCommand().replace("%player%", first.getName()));
                }
            }
        }
    }

    private void sendMessage(String message) {
        Bukkit.getOnlinePlayers().forEach(players -> players.sendMessage(ChatUtils.getPrefix() + message));
    }

    private void sendPrivateMessage(String message) {
        Hunter.getPlayers().forEach(hunters -> hunters.getPlayer().sendMessage(ChatUtils.getPrefix() + message));
    }

    private boolean allHuntersHaveTheSameScore() {
        if (Hunter.getPlayers().isEmpty()) return true;
        int score = Hunter.getPlayers().get(0).getScore();

        for (int i = 1; i < Hunter.getPlayers().size(); i++) {
            Hunter hunter = Hunter.getPlayers().get(i);
            if (hunter.getScore() != score) return false;
        }

        return true;
    }

    private Player getFirst() {
        int score = 0;
        Player player = null;

        for (Hunter hunters : Hunter.getPlayers()) {
            if (hunters.getScore() > score) {
                score = hunters.getScore();
                player = hunters.getPlayer();
            }
        }

        return player;
    }

    /* private boolean day(String name) {
        long time = Bukkit.getWorld(name).getTime();
        return time < 12300 || time > 23850;
    } */
}
