package fr.ziedelth.mobhunt.listeners;

import fr.ziedelth.mobhunt.listeners.entity.EntityDeath;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

public class EventManager {
    public EventManager(Plugin plugin) {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new EntityDeath(), plugin);
    }
}
