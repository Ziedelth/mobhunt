package fr.ziedelth.mobhunt.listeners.entity;

import fr.ziedelth.mobhunt.MobHunt;
import fr.ziedelth.mobhunt.utils.Hunter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EntityDeath implements Listener {
    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();
        Player killer = entity.getKiller();

        if (killer != null && MobHunt.getInstance().isActivated() && MobHunt.getInstance().contains(killer) && MobHunt.getInstance().getWorlds().contains(killer.getWorld().getName())) {
            Hunter hunter = Hunter.get(killer);
            int score = hunter.addMobKill(entity);

            if (score > 0) {
                killer.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder("+" + score + " points").color(ChatColor.GREEN).bold(true).create());
                killer.playSound(killer.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1f, 1f);
            }
        }
    }
}
